# Eagle-200 Exporter

This exports [Prometheus](https://prometheus.io) metrics from the
[Eagle-200](https://rainforestautomation.com/us-retail-store/eagle-200-smart-meter-energy-gateway/)
energy meter gateway.

A simple Python script is used to periodically query the Eagle-200 using the
documented [local
API](https://rainforestautomation.com/wp-content/uploads/2017/02/EAGLE-200-Local-API-Manual-v1.0.pdf).
An HTTP server is used to provide these values in metrics for Prometheus. The
metrics can be graphed using [Grafana](https://grafana.com).

## Running

The package can be built using [`build`](https://github.com/pypa/build):

```bash
python3 -m venv --upgrade-deps .venv
.venv/bin/pip install build
.venv/bin/python3 -m build
```

It is a pure Python wheel and depends on `prometheus_client`, `requests`, and
`defusedxml`.

```bash
.venv/bin/pip install dist/eagle_exporter-*-py3-none-any.whl
```

The configuration is set via environment variables:

```bash
# This is the IPv4 address of the Eagle-200 device
export EAGLE_ADDR=192.0.2.1
# The cloud ID is printed on the bottom and is the last 6 hex digits of the MAC address. This is used as the username when connecting.
export EAGLE_CLOUD_ID=xxxxxx
# The install code is printed on the bottom and is 16 hex digits. This is used as the password when connecting.
export EAGLE_INSTALL_CODE=xxxxxxxxxxxxxxxx
.venv/bin/eagle_exporter
```

The HTTP server listens on `0.0.0.0:9708/metrics`.

## License

This work is available under the [MIT License](LICENSE).

## References

These are some resources I consulted that may be helpful:

- https://rainforestautomation.com/wp-content/uploads/2017/02/EAGLE-200-Local-API-Manual-v1.0.pdf
- https://kellyhirano.tumblr.com/post/170232890722/iot-project-instantaneous-power-demand-with
- https://github.com/sbrudenell/eagle_exporter
- https://github.com/gtdiehl/eagle200_reader
