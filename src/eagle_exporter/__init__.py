"""Eagle-200 Prometheus exporter."""

import logging
import os
import typing

from prometheus_client import start_http_server

from .eagle200 import Eagle200API
from .exporter import continually_scrape

EXPORTER_PORT = 9708


def main() -> typing.NoReturn:
    """Run the HTTP server and scrape the metrics."""
    logging.basicConfig(
        style="{",
        level=logging.INFO,
        format="{asctime} {levelname} {message}",
        datefmt="%Y-%m-%d %H:%M:%S%z",
    )
    logging.info(f"Starting HTTP server on 0.0.0.0:{EXPORTER_PORT}")
    start_http_server(EXPORTER_PORT)

    eagle_addr = os.environ["EAGLE_ADDR"]
    cloud_id = os.environ["EAGLE_CLOUD_ID"]
    install_code = os.environ["EAGLE_INSTALL_CODE"]

    logging.info(f"Connecting to Eagle-200 at {eagle_addr}")
    eagle = Eagle200API(eagle_addr, cloud_id, install_code)
    continually_scrape(eagle)
