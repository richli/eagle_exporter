"""Eagle-200 Prometheus exporter."""

import logging
import signal
import sys
import time
from types import FrameType
from typing import Final, NoReturn, Optional

from prometheus_client import Gauge, Info

from .eagle200 import Component, Device, Eagle200API

SCRAPE_TIME = Gauge(
    "eagle_scrape_seconds",
    "Last time the Eagle-200 gateway was queried in seconds since unix epoch",
)

INSTANT_DEMAND = Gauge(
    "eagle_instantaneous_demand_watts",
    "Instantaneous power usage in W",
    labelnames=[
        "device",
        "component",
    ],
)

SUM_DELIVERED = Gauge(
    "eagle_current_summation_delivered_joules",
    "Summation of energy delivered to home in J",
    labelnames=[
        "device",
        "component",
    ],
)

MESSAGE_INFO = Info(
    "eagle_message", "Message from the utility", labelnames=["device", "component"]
)

DEVICE_STATUS = Info(
    "eagle_device",
    "Eagle-200 device info",
    labelnames=[
        "device",
    ],
)


def update_device_stats(device: Device) -> None:
    """Update the defined Prometheus metrics for the given device."""
    status_gauge = DEVICE_STATUS.labels(
        device=device.name,
    )
    status_gauge.info(
        {
            "connection_status": device.connection_status,
            "hardware_address": device.hardware_address,
            "name": device.name,
            "model_id": device.model_id,
            "last_contact": str(device.last_contact),
        }
    )
    if device.connection_status != "Connected":
        logging.warning(f"Connection status: {device.connection_status}")


def update_component_stats(device: Device, component: Component) -> None:
    """Update the defined Prometheus metrics for the given component."""
    KILOWATTS_TO_WATTS: Final = 1000
    KILOWATTHOURS_TO_JOULES: Final = 60 * 60 * 1000

    demand = component.variables.get("zigbee:InstantaneousDemand")
    delivered = component.variables.get("zigbee:CurrentSummationDelivered")
    message = component.variables.get("zigbee:Message")

    if demand is not None:
        if demand.units is None or demand.units != "kW":
            logging.warning("Instantaneous demand units are unexpected")

        if demand.value is None:
            logging.warning("No instantaneous demand value found")
        else:
            demand_gauge = INSTANT_DEMAND.labels(
                device=device.name, component=component.name
            )
            demand_gauge.set(float(demand.value) * KILOWATTS_TO_WATTS)
            logging.info(f"Instantaneous demand: {demand.value} kW")
    else:
        logging.warning("No instantaneous demand variable found")

    if delivered is not None:
        if delivered.units is None or delivered.units != "kWh":
            logging.warning("Sum of delivered power units are unexpected")

        if delivered.value is None:
            logging.warning("No sum of delivered power value found")
        else:
            delivered_gauge = SUM_DELIVERED.labels(
                device=device.name, component=component.name
            )
            delivered_gauge.set(float(delivered.value) * KILOWATTHOURS_TO_JOULES)

            logging.info(f"Sum of delivered power: {delivered.value} kWh")
    else:
        logging.warning("No sum of delivered power variable found")

    if message is not None:
        if message.value is not None:
            message_gauge = MESSAGE_INFO.labels(
                device=device.name, component=component.name
            )
            message_gauge.info({"message": message.value})

            logging.info(f"Message: {message.value}")
    else:
        logging.warning("No message variable found")


def sigterm_handler(_signum: int, _stack_frame: Optional[FrameType]) -> NoReturn:
    """Gracefully exit when SIGTERM is received."""
    sys.exit(0)


def continually_scrape(eagle: Eagle200API) -> NoReturn:
    """Continually query the gateway and update the metrics."""
    devices = eagle.get_device_list()
    logging.info(f"Number of devices reported: {len(devices)}")

    # When SIGTERM is received, just quit immediately
    signal.signal(signal.SIGTERM, sigterm_handler)

    while True:
        logging.debug("Querying gateway")
        SCRAPE_TIME.set_to_current_time()
        for device in devices:
            device_data = eagle.device_query(device)
            if device_data is None:
                logging.warning("No device data returned")
                continue

            update_device_stats(device)
            for component in device_data.components:
                update_component_stats(device, component)

        time.sleep(20)
