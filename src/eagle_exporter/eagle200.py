"""Eagle-200 API.

Refer to:

https://rainforestautomation.com/wp-content/uploads/2017/02/EAGLE-200-Local-API-Manual-v1.0.pdf
"""
from __future__ import annotations

import logging
from dataclasses import dataclass
from typing import TYPE_CHECKING, Optional

if TYPE_CHECKING:
    from xml.etree.ElementTree import Element

import defusedxml.ElementTree as ET
import requests


def find_default(e: Optional[Element]) -> str:
    """Return a default string no matter if the input is valid or not."""
    if e is None:
        return "Unknown"
    else:
        if e.text is None:
            return "Unknown"
        else:
            return e.text


def find_none(e: Optional[Element]) -> Optional[str]:
    """Extract the element text if it exists, otherwise None."""
    if e is None:
        return None
    else:
        return e.text


@dataclass
class Device:
    """A device reported by the Eagle-200."""

    name: str
    hardware_address: str
    network_address: str
    model_id: str
    connection_status: str
    last_contact: int

    @staticmethod
    def from_xml(e: Element) -> Device:
        """Construct a new `Device` from an XML element and its children."""
        name = find_default(e.find("Name"))
        model_id = find_default(e.find("ModelId"))
        hw_addr = find_default(e.find("HardwareAddress"))
        last_contact = find_default(e.find("LastContact"))
        conn_status = find_default(e.find("ConnectionStatus"))
        net_addr = find_default(e.find("NetworkAddress"))

        return Device(
            name=name,
            last_contact=int(last_contact, 16),
            hardware_address=hw_addr,
            network_address=net_addr,
            model_id=model_id,
            connection_status=conn_status,
        )


@dataclass
class DeviceVariable:
    """A variable for a component of a device."""

    name: str
    value: Optional[str]
    units: Optional[str]
    description: Optional[str]

    @staticmethod
    def from_xml(e: Element) -> Optional[DeviceVariable]:
        """Try to construct a `DeviceVariable` from an XML element and its children."""
        name_element = e.find("Name")
        value_element = e.find("Value")
        units_element = e.find("Units")
        desc_element = e.find("Description")

        return DeviceVariable(
            name=find_default(name_element),
            value=find_none(value_element),
            units=find_none(units_element),
            description=find_none(desc_element),
        )


@dataclass
class Component:
    """A component of a device."""

    name: str
    variables: dict[str, DeviceVariable]

    @staticmethod
    def from_xml(e: Element) -> Optional[Component]:
        """Try to construct a new `Component` from an XML element and its children."""
        name_element = e.find("Name")
        variables_element = e.find("Variables")
        if name_element is None or variables_element is None:
            return None

        fallible_variables = (
            DeviceVariable.from_xml(v) for v in variables_element.findall("Variable")
        )
        variables = {v.name: v for v in fallible_variables if v is not None}
        return Component(name=find_default(name_element), variables=variables)


@dataclass
class DeviceData:
    """A device and its components."""

    device: Device
    components: list[Component]

    @staticmethod
    def from_xml(e: Element) -> Optional[DeviceData]:
        """Try to construct a new `DeviceData` from an XML element and its children."""
        device_details = e.find("DeviceDetails")
        device_components = e.find("Components")
        if device_details is None or device_components is None:
            return None

        device = Device.from_xml(device_details)
        fallible_components = (
            Component.from_xml(c) for c in device_components.findall("Component")
        )
        components = [c for c in fallible_components if c is not None]
        return DeviceData(device, components)


class Eagle200API:
    """Interact with the Eagle-200 gateway through its local API."""

    def __init__(self, addr: str, cloud_id: str, install_code: str) -> None:
        """Set the internal parameters needed to interact with the gateway."""
        self.url = f"http://{addr}/cgi-bin/post_manager"

        self.session = requests.Session()
        self.session.auth = (cloud_id, install_code)
        self.session.headers.update({"Content-Type": "text/xml"})

    def get_device_list(self) -> list[Device]:
        """Query the Eagle for all its devices."""
        query = "<Command><Name>device_list</Name></Command>"
        r = self.session.post(self.url, query)
        try:
            r.raise_for_status()
        except requests.HTTPError:
            logging.exception("Device list query failed")
            return []

        tree = ET.fromstring(r.text)
        return [Device.from_xml(d) for d in tree.findall("Device")]

    def device_query(self, device: Device) -> Optional[DeviceData]:
        """Query the Eagle for details about a device."""
        query = f"""<Command>
                   <Name>device_query</Name>
                   <DeviceDetails>
                   <HardwareAddress>{device.hardware_address}</HardwareAddress>
                   </DeviceDetails>
                   <Components><All>Y</All></Components>
                   </Command>"""
        r = self.session.post(self.url, query)
        try:
            r.raise_for_status()
        except requests.HTTPError:
            logging.exception("Device query failed")
            return None

        tree = ET.fromstring(r.text)
        return DeviceData.from_xml(tree)
