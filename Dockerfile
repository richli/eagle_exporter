# Eagle-200 exporter Dockerfile
#
# Build with:
#
# docker build -t eagle_exporter:latest \
#	--build-arg=build_date=$(date --utc --date="@${SOURCE_DATE_EPOCH:-$(date +%s)}" --rfc-3339='seconds') \
#	--build-arg=version=$(git rev-parse --short HEAD) \
#	--build-arg=revision=$(git symbolic-ref --short HEAD) \
#   .
#
# Run with the following environment variables set:
#
# EAGLE_ADDR
# EAGLE_CLOUD_ID
# EAGLE_INSTALL_CODE

FROM docker.io/library/python:3.11-slim AS build
WORKDIR /usr/src/app

COPY README.md LICENSE pyproject.toml ./
COPY src/ ./src/

RUN python3 -m venv --upgrade-deps .venv && \
    .venv/bin/pip install build
RUN .venv/bin/python -m build

FROM docker.io/library/python:3.11-slim
WORKDIR /usr/src/app

COPY --from=build \
    /usr/src/app/dist/eagle_exporter-*-py3-none-any.whl \
    /usr/src/app/

RUN python3 -m venv --upgrade-deps .venv && \
    .venv/bin/pip install --no-cache-dir eagle_exporter-*-py3-none-any.whl

EXPOSE 9708
ENTRYPOINT ["/usr/src/app/.venv/bin/eagle_exporter"]
ENV PYTHONWARNINGS=default

ARG version
ARG revision
ARG build_date
LABEL maintainer="Richard Lindsley <rich.lindsley@gmail.com>" \
    "org.opencontainers.image.title"="Eagle-200 metrics exporter" \
    "org.opencontainers.image.description"="Produce Prometheus metrics for the Eagle-200 energy monitoring gateway" \
    "org.opencontainers.image.created"="$build_date" \
    "org.opencontainers.image.authors"="Richard Lindsley <rich.lindsley@gmail.com>" \
    "org.opencontainers.image.url"="https://gitlab.com/richli/eagle_exporter" \
    "org.opencontainers.image.source"="https://gitlab.com/richli/eagle_exporter" \
    "org.opencontainers.image.version"="$version" \
    "org.opencontainers.image.revision"="$revision"
